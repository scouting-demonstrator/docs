# Configuration management

The scouting FM configuration in the [repository](https://gitlab.cern.ch/scouting-demonstrator/scouting-fm-configuration) is stored in multiple files to keep management tracktable.

## Organisation

The folder `duck` contains template xml files that are filled by the continuous integration system using the `scripts/assemble_fm_parameters_into_xml.py` script. The CI also automatically fills the RCMS and scoutFM version based on the target FM (the mechanism how this is achieved is explained later).

The above mentioned script scans through each xml file in `duck/` and replaces strings of the form `$(filename)$` with the contents of `filename`. Optionaly, the suffix `.noescape` can be appended to the filename, i.e. `$(filename.noescape)$` in which case special JSON characters won't be escaped. The script by default searches the folder `config_fragments` for files with the given name, but if it is run locally an arbitrary search path can be passed.

### Example

The file [duck/scout_l1scoutpro_template.xml](https://gitlab.cern.ch/scouting-demonstrator/scouting-fm-configuration/-/blob/9964dadd77f4182459c9f111f04c6378948949d3/duck/scout_l1scoutpro_template.xml) contains

```json
{
    "host": "scoutctrl-s1d12-18-01",
    "port": "8080",
    "name": "vcu128_ugmtbmtf",
    "index": "0",
    "pipelines": [
        {
            "name": "ugmt",
            "associatedFED": "1402",
            "daq_units": [
                {
                    "source_ip": "10.177.128.190",
                    "id": 0,
                    "tcp_streams": [
                        {"dest_port" : 10000, "source_port" : 10000, "tcp_stream_id" : 0, "source_id" : 1, processor_type: "GMT", primitive_type: "MUON"}
                    ]
                }
            ],
            "ru_config": {
                "port": 8000,
                "base_config": $(scdaq_base_config.json5.noescape)$
            }
        }
    ]
}
```

where the majority of data is already present, but the `base_config` of the RU (i.e. SCDAQ) has been moved out to the file `config_fragments/scdaq_base_config.json5`. The `.noescape` suffix indicates that the file contents will be pasted as-is into the xml file, so in this case the existing JSON will simply be expanded by the contents of `config_fragments/scdaq_base_config.json5`. In contrast, in

```json
"jsd": [
    {
        "name": "EndOfLumiSectionFile",
        "filename": "EoLS.jsd",
        "contents": $(EoLS_template.jsd)$
    },
    {
        "name": "EndOfRunFile",
        "filename": "EoR.jsd",
        "contents": $(EoR_template.jsd)$
    },
    {
        "name": "RawDataFile",
        "filename": "rawData.jsd",
        "contents": $(rawData_template.jsd)$
    }
],
```

the content of e.g. `EoLS_template.jsd` will be processed using `json.dumps()` which escapes special JSON characters in such a way that the output can be stored in a JSON string.
