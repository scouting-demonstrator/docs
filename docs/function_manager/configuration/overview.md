# Overview

The scouting FM is responsible for controlling the scouting boards, the receiver units (i.e. the SCDAQ application), the buffer units (hltd), and the processing units (hltd, CMSSW).

To do so in a reasonably flexible way we store the FM configuration as JSON inside the RS3 DB. It is split into four parts, each stored as a property of the function manager:

* `LEGACY_SCOUTING_UNIT_CONFIG`
    * `fedID`: indicates which FED ID this scouting unit is receiving data from
    * `scdaq`: contains the host/port as well as (optionally) the configuration of the SCDAQ process that receives data from the board
    * `scone`: contains the host/port of the machine running SCONE as well as the board name that should be controlled. Additionally contains
        * `monitorables`: lists of registers along with expected values for various states (running, configured)
        * `settings`: lists of registers along with values that should be written to them at various state transitions (start, stop, configure)
* `BOARD_CONFIG`
    * Contains host/port of the machine running SCONE as well as board name and index to address a specific board
    * `pipelines`: a list of scouting pipelines (i.e. units within a board that take data from a given FED ID) and the configuration of their associated receiver unit (SCDAQ)
        * `daq_units`: a list of units associated to a pipeline, each of which establishes an individual TCP/IP connection
            * `tcp_streams`: a list of TCP streams, each indicating which ports are used and which kind of objects are transported on them. This information is both to configure the scouting board as well as the receiver unit (SCDAQ)
* `RECEIVER_BUFFER_UNIT_CONFIG`
    * `hosts`: list of hosts acting as RUBUs
    * `initial_config_storage_target`: temporary directory in which the hltd configuration is stored at `configure`
    * `base_run_directory`: directory where the run directory (containing the hltd config and the SCDAQ output files) will be created at `start`
    * `prescales`: prescales that will be set based on the runkey passed to the FM (either manually at configure or automatically by the L0 as configured in the L1CE)
    * `config_directories`: the contents of the hltd config directories (`jsd` and `hlt`)
    * `setupmachine_options`: the commands passed to the `setupmachine.py` script on all RUBUs at `init`
* `PROCESSING_UNIT_CONFIG`
    * `hosts`: list of hosts acting as PUs
    * `setupmachine_options`: the commands passed to the `setupmachine.py` script on all PUs at `init`

The function manager configuration is stored separately from the function manager in the source control repository at [scouting-demonstrator/scouting-fm-configuration](https://gitlab.cern.ch/scouting-demonstrator/scouting-fm-configuration).
