# Configuration change

## tl;dr

If you want to change the configuration for the function manager version v15 (using RCMS version 4_9_3) you should commit to the branch named `scoutFM-RCMS_4_9_3_SCOUTFM_v15`. An archive containing the xml configuration files will be automatically created in the [package repository](https://gitlab.cern.ch/scouting-demonstrator/scouting-fm-configuration/-/packages) from where it can either be retrieved manually or (preferred) deployed using the script at P5 as described on the page for the [deployment of the FM](../deployment.md).

For a more detailed description, read on.

## Overview

The CI of the configuration repository is triggered in three ways:

* when the [pipeline for the function manager itself](https://gitlab.cern.ch/cms-rcms/functionmanagers/scout/-/pipelines) succeeds
* automatically upon each commit when changing the configuration of a tagged FM
* manually when changing the configuration of a FM that is being developed in a branch

## Triggered by FM repository

When a new version of the FM is built by the CI in the [FM repository](https://gitlab.cern.ch/cms-rcms/functionmanagers/scout) that pipeline will attempt to create a pipeline in a branch of the **name of the branch or tag** of the FM. If no such branch is found in the [FM configuration repository](https://gitlab.cern.ch/scouting-demonstrator/scouting-fm-configuration) the job will fail.

### Example

If the FM is built in branch `test-new-feature`, then a branch with the same name should exist in the configuration repository. Likewise, if the FM is tagged, e.g. as `scoutFM-RCMS_4_9_3_SCOUTFM_v15`, then a **branch** of the same name needs to exist in the configuration repository.

## Triggered by changes to the configuration of a **tagged** FM

If the configuration in the branch corresponding to a tag (i.e. a branch of the form `scoutFM-RCMS_*`) is changed by commiting to that branch, the CI pipeline is automatically triggered and creates a new configuration that supersedes the previous one. Once the CI pipeline succeeds it is ready to be picked up by the deployment script at P5 (see [deployment of the FM](../deployment.md)).

### Example

If the a new commit is made to branch `scoutFM-RCMS_4_9_3_SCOUTFM_v15` the CI pipeline will automatically extract the required information and create a new configuration that will be stored in the [package repository](https://gitlab.cern.ch/scouting-demonstrator/scouting-fm-configuration/-/packages).

## Triggered manually for changes to the configuration of a development (i.e. untagged) FM

If a new commit is made to a branch that doesn't conform to the naming convention for FM tags (i.e. it does **not** begin with `scoutFM-RCMS_`), a pipeline will be created, but not executed. To successfully execute it, the CI variables `RCMS_VERSION` and `FM_VERSION` corresponding to the values of the associated FM have to be supplied.

### Example

If a new commit is made to branch `refactor-move-out-configuration` the pipeline is initially waiting to be started manually. To start it, one can click on the left cog and then on the name of the job ("get_vars_from_repo"), but **not** onto the play symbol.

![Manual job waiting to be started](figures/manual_job_waiting.png)

In the subsequent page the required variables (corresponding to the FM versions) need to be entered before the job can be started:

![Manual job creation page](figures/manual_job_creation.png)

Once the pipeline has completed the archive of the configuration can again be found in the [package repository](https://gitlab.cern.ch/scouting-demonstrator/scouting-fm-configuration/-/packages).
