# Deployment of the scouting function manager

## Preparation

After the function manager as well as the associated configuration has been built they can be prepared for deployment at P5 with the help of the `deploy_scoutFM.sh` script stored in the NFS home directory of the `scouter` user.

The script can either retrieve only the latest configuration for a given FM or both the latest configuration as well as the jar file of the FM. It needs to be run with the following arguments

```
deploy_scoutFM.sh [Gitlab username] [RCMS version] [scoutFM package version]
```

Executing the script will do two things:

1. copy the configuration files to `~/scoutFM/${RCMS_VERSION}/${SCOUTFM_VERSION}/` (e.g. `~/scoutFM/RCMS_4_9_3/SCOUTFM_v15/`)
2. (optionally) retrieve the jar (i.e. Java executable) file for the function manager and
    * copy it to `~/scoutFM/${RCMS_VERSION}/${SCOUTFM_VERSION}`
    * also deploy it to `~rcms/centralRcmsServices/staticContent/functionmanagers/${RCMS_VERSION}/`

### Example

```
[dinyar@kvm-s3562-2-ip157-08 (cmsusr21) dinyar]$ ~scouter/deploy_scoutFM.sh dinyar RCMS_4_9_3 SCOUTFM_v15

####### WARNING: This script will only work with FM versions LATER than v14. #######

~/scoutFM/RCMS_4_9_3/SCOUTFM_v15 /cmsnfshome0/nfshome0/dinyar
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current  Dload  Upload   Total   Spent    Left  Speed
100  6848  100  6848    0     0  27612      0 --:--:-- --:--:-- --:--:-- 27502
scoutFM-RCMS_4_9_3_SCOUTFM_v15_duck/scout_l1scoutdev_layer2-ugmt.xml
scoutFM-RCMS_4_9_3_SCOUTFM_v15_duck/scout_l1scoutdev_ugt-bmtf.xml
scoutFM-RCMS_4_9_3_SCOUTFM_v15_duck/scout_l1scoutdev.xml
scoutFM-RCMS_4_9_3_SCOUTFM_v15_duck/scout_l1scoutpro.xml
### Extracted configuration in /nfshome0/dinyar/scoutFM/RCMS_4_9_3/SCOUTFM_v15. Do you also want to download and deploy the function manager jar file (you need the correct permissions for this)? [y/n] y
Downloading FM jar..
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 7196k  100 7196k    0     0  27.4M      0 --:--:-- --:--:-- --:--:-- 27.4M
Copying jar to RCMS FM repository..
All done. Files were extracted in /nfshome0/dinyar/scoutFM/RCMS_4_9_3/SCOUTFM_v15.
```

## Deploying the FM and its configuration

The FM configuration needs to be added to the RS3 database in order to it to be available for construction by the Tomcat. This can be done using the RS3 Manager application, most conveniently via the X2Go service.

After starting the RS3 Manager application you will need to login with the credentials that were given to you. Once you have done so, several "RS3 users" will appear on the right side. You should select either `l1scoutpro` or `l1scoutdev` depending on whether you want to test a new FM (configuration) or deploy it to the production system. You do so by single clicking on the entry.

![RS3 Manager login screen](figures/rs3manager_login.png)

Following the successfull login you need to move to the "Duck configurator" tab. 

![RS3 Manager duck configurator](figures/rs3manager_duck.png)

Here you chose the xml configuration file to use by clicking on the folder icon, followed by clicking on the duck icon. After entering a description of the changes introduced by this version the new configuration is stored in the DB and can be found by moving to the "Viewer/Editor" tab.

![RS3 Manager viewer](figures/rs3manager_viewer.png)

The CI pipeline automatically assigns a configuration to the current year and appends the shortened Git SHA of the associated commit to the configuration name to make tracking of changes easier (e.g. here the year 2024 and the SHA 62855bb9).

### Deployment for the global run

To register a given configuration to be used for the global run you need to change to the "Global Configuration Map" panel where you (after connecting to the GCM database) select the `CMS/CENTRAL/GLOBAL_RUN` configuration map as well as the scouting FM configuration you would like to use for the next global runs (in the example given this is v15 with the SHA given in the above section). Once both are selected you can click "Register".

![RS3 Manager Global Configuration Map](figures/rs3manager_gcm.png)

**Important:** After the registration is done you need to request a red-recycle from the shift crew at P5 in order to the changes to be picked up!
