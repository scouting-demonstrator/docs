# scoutFM

The RCMS function manager for the L1SCOUT subsystem is written in Java and communicates with the components of the scouting system in a variety of ways:

* SCONE: Rest interface
* SCDAQ: Raw sockets (to be migrated to a Rest interface)
* Run directory cleaner: Rest interface
* CMSSW: Shell commands via SSH

It also writes the configuration for SCDAQ and hltd/CMSSW to the respective hosts via SSH.

![System structure](figures/system_structure_sketch.png)

The source code of the function manager is stored in [cms-rcms/functionmanagers/scout/](https://gitlab.cern.ch/cms-rcms/functionmanagers/scout/).

## Programmatic structure

When the function manager is created (either manually or by the L0 FM) the `createAction(...)` in the `MyFunctionManager` class is called. The in turn retrieves the following configurations from the RS3 DB:

* Legacy scouting units
* Scouting units
    * Settings for a scouting board as well as board-specific settings for its associated receiver unit(s)
* Receiver/buffer unit
    * Hosts acting as RUBUs as well as their non-board specific configuration
* Processing unit
    * Hosts acting as PUs as well as their configuration

These data are stored in the following classes:

* `ScoutingUnit`
    * implements the `ScoutingUnitInterface`
    * contains:
        * a list of faulty RUBUs (as determined at `configure`)
        * two lists of `ScoutingPipeline`: the associated pipelines and the pipelines currently included in the run
            * `ScoutingPipeline`
                * contains:
                    * associated FED ID
                    * `ScoutingDaqUnit`
                        * contains:
                            * the configuration of an individual DAQ unit within a scouting pipeline
                            * a list of `ScoutingTcpStream`
                                * stores the configuration settings needed by either the board or SCDAQ to handle a given TCP stream
                    * `ScdaqControl`
                        * contains:
                            * the host/port running a given instance of SCDAQ
                            * the associated buffer unit if different from the above host
                            * the SCDAQ configuration
                            * the configuration for all associated TCP streams
                            * a list of associated `ScoutingDaqUnit`
        * `SconeConfV2`
            * implements the `ScoutingBoard` interface
            * controls boards that exploit the new SCONE interface such as VCU128
            * contains:
                * host/port of machine running SCONE
                * data of controlled board (name and index)
                * list of `ScoutingPipeline` included in the current run
* `ReceiverBufferUnit`
    * contains:
        * hostname/port of a RUBU machine
        * configuration for hltd and CMSSW
        * a map of prescales to use for a given run key
        * the name of the main BU (i.e. the one running the file broker)
        * which BUs are included in the current run
* `ProcessingUnit`
    * stores hosts that act as PUs and share the same configuration
* `LegacyScoutingUnit`
    * legacy class currently only used to control Micron SB852 board in charge of BRIL scouting
    * implements the `ScoutingUnitInterface`
    * contains:
        * the FED ID the unit is scouting from
        * `SconeConf`
            * to control legacy boards
            * implements the `ScoutingBoard` interface
            * contains:
                * host/port of the machine running SCONE
                * name of the controlled board
                * settings to write at configure/start/stop transition
                * monitorables to read when configured/running
        * `ScdaqControl`
            * see above

``` mermaid
classDiagram

class ScoutingUnitInterface{
    <<interface>>
    +enum State
    +enum ErrorType
    +addBoardControl(ScoutingBoard)
    +checkState() State
    +getFaultyRUBUs() List~ReceiverBufferUnit~
    +configure(fedEnableMask, runKey, cmssw_version, cmssw_sw_arch, bufferUnitsInRun) State
    +start(runNumber) State
    +stop()
    +halt()
    +reset()
}

class LegacyScoutingUnit{
    -fedID
}

class ScoutingUnit

class ScoutingPipeline{
    -fedID
    +configure(bufferUnitsInRun, runKey, cmssw_version) State
    +start(runNumber) State
    +stop(runNumber) State
}

class ScoutingDaqUnit{
    -sourceIP
    +getTcpStreams() List~ScoutingTcpStream~
    +getSconeConfigFragment() ObjectNode
    +getScdaqConfigFragment() ArrayNode
}

class ScoutingTcpStream{
    -destPort
    -sourcePort
    -streamID
    -sourceID
    -processorType
    -primitiveType
}

class ScoutingBoard{
    <<interface>>
    +configure() State
    +start() State
    +stop() State
}

class SconeConf{
    -host
    -port
    -board
    -configSettings
    -startSettings
    -stopSettings
}

class SconeConfV2{
    -host
    -port
    -board
    -index
}

class ScdaqControl{
    -host
    -port
    -associatedBufferUnit
    -config
    +configure(daqUnits) State
    +start(runNumber) State
    +stop(runNumber) State
}

class ReceiverBufferUnit{
    -hostname
    -jsd_files List~HltdConfigFile~
    -hlt_files List~HltdConfigFile~
    -mainHost
    +initialise() State
    +configure(bufferUnitsInRun, runKey, cmssw_version, cmssw_scram_Arch) State
    +start(runNumber) State
}

class ProcessingUnit{
    -hostnames List~String~
    -setupmachine_options List~String~
    +addHostname(host)
    +addSetupmachineOption(option)
    +configure() State
}

ScoutingUnitInterface <|-- LegacyScoutingUnit
LegacyScoutingUnit *-- ScdaqControl
LegacyScoutingUnit *-- SconeConf

ScoutingUnitInterface <|-- ScoutingUnit
ScoutingUnit *-- SconeConfV2
ScoutingUnit "1" *-- "1..*" ReceiverBufferUnit : faulty
ScoutingUnit "1" *-- "1..*" ScoutingPipeline : associated
ScoutingUnit "1" *-- "1..*" ScoutingPipeline : in run
ScoutingPipeline  *-- ReceiverBufferUnit
ReceiverBufferUnit "1" *--* "1..*" ReceiverBufferUnit : in run
ScoutingPipeline "1" *-- "1..*" ScoutingDaqUnit
ScoutingDaqUnit "1" *-- "1..*" ScoutingTcpStream
ScoutingPipeline  *-- ScdaqControl
ScdaqControl "1" *-- "1..*" ScoutingDaqUnit

ScoutingBoard <|-- SconeConf
ScoutingBoard <|-- SconeConfV2
SconeConfV2 "1" *-- "1..*" ScoutingPipeline : in run


```

## State machine actions

This section gives an overview of the important FSM actions (`reset` etc. are ommited) that are defined in the `MyEventHandler` class.

When `initAction(...)` is called the processing units are configured and the buffer units are initialised (i.e. in both cases the `setupmachine.py` script is executed with the commands from the configuration and hltd is restarted).

`configureAction(...)` first associates RUBUs found in the configuration to each scouting unit in the run. The way this works is that we loop over all scouting units (checking whether they are in a list of defunct scouting units in which case we skip them) and within a scouting unit over all pipelines that it contains. If the FED associated to a pipeline is in the run we take a RUBU from the list of available RUBUs (checking whether that RUBU is in the list of defunct RUBUs in which case we skip it). 

*Note:* If no RUBU is left to associate the scouting unit will remain uninitialised and will not take part in the run. 

A list of RUBUs that are included in the run is created which is needed to configure CMSSW (so processes know where data is being made available). *Note:* The RUBU that is first in the above list will be picked as the host that runs the file broker. In order to ensure that CMSSW operates only on scouting events (i.e. orbits) that have data from each scouted system, this "main BU" needs to be started last and stopped first. (In this way the file broker is only started once all other units are also providing data, and vice versa at stop.)

After this association step, each (non-defunct) scouting unit is configured. This means that the full chain of board, receiver unit (SCDAQ), and buffer unit (hltd and CMSSW) are configured. In case the configuration of a scouting unit fails, the unit is queried whether this failure was due to the board or RUBU and either the unit or the RUBU are added to the list of defunct units/RUBUs and the entire configuration step (including scouting unit-RUBU association) is repeated (up to two times). 

Finally, any legacy scouting units are configured as well.

The `startAction(...)` method starts all scouting units that are included in the run in reverse order (compared to configuration order). This is done to satisfy the above mentioned requirement that the main BU should be started last. Legacy scouting units are started in standard order.

In `stopAction(...)` (legacy) scouting units are stopped in the standard order as this already ensures that the main BU is stopped first.

*Note:* Currently a temporary workaround is implemented in `stopAction(...)` where all scouting units are (re)configured after stop which causes the boards to be fully reset and SCDAQ to be restarted. This is needed due to bugs present in both the firmware and SCDAQ.

<!-- TODO: ## Manual operation of the function manager -->

<!-- TODO: ## Explanation of SU, RUBU, etc. classes? -->
