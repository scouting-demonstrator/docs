# Operation in local

The scouting FM can be operated in local for tests etc. via the [development instance of the FM](http://cmsrc-l1scoutdev.cms:43000/rcms/) from within the CMS technical network. Once logged in (ask on Mattermost if you require the username/password) several folders containing the FM configurations are shown:

![The RCMS configuration chooser](figures/rcms_configuration-chooser.png)

Our latest configurations are shown in the folder "l1scout", a good default choice is "levelOneFMtestL1SCOUT" which supports all available scouting boards.

**Note:** Before proceding please check if there are any running configurations left and destroy them if you are sure that nobody else is using the system. (This is described in a later section.)

## Creating a new function manager

In the configuration chooser you can click on your chosen FM configuration which will you allow to either "Create" a new FM oder "Attach" yourself to a running configuration. If there are no running configurations only the "Create" button should work. 

### Initialisation

Once clicked you will be shown the rudimentary FM control interface:

![The scouting FM in state "Initial"](figures/rcms_fm-init.png)

At this stage you can simply click on the button "Initialize" which will cause the function manager to initialise the hltd processes on the RUBUs and PUs. It will also make the FM obtain a session ID that is later used to retrieve an official run number.

**Important:** Please do NOT enable the "GLOBAL_CONF_KEY" or "SID" fields via the Command Parameter Section unless you are very sure you know what you are doing.

### Configuration

At the configure step only the FED enable mask needs to be entered. This can be done by ticking the box next to "Show Command Parameter Section" and the box for the "FED_ENABLE_MASK":

![The scouting FM in state "Halted"](figures/rcms_fm-config.png)

In the example screenshot the value `1402&3%1360&3` is entered which indicates that FEDs 1402 (uGMT) and 1360 (Calo Layer-2, Demux) are included in the run. Adjust this to your needs and click on the "Configure" button.

### Start

To start you will simply need to click on the "Start" button which will cause the FM to obtain an official run number.

**Important:** Please do not enter a run number yourself via the Command Parameter Section unless you are absolutely certain you don't want an official run number.

### Stop etc.

While running you can then stop the run via "Stop", move back to "Halted" via "Halt" or "Reset" etc.

## Connecting to a running configuration

It may be that you want to reconnect to an already running configuration (e.g. because your connection failed and you would like to reconnect). To do so click on "Running Configurations" in the left sidebar and click on the link provided (e.g. `/l1scoutdev/l1scout/levelOneFMtestL1SCOUT`). 
