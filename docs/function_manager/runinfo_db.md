# Access to the Run Info database

The Run Info database is used by the scouting system to store various configuration data. It can be accessed using the [RunInfoServlet](http://cmsrc-srv.cms:9500/RunInfoServlet/) from within the CMS technical network.

![The Run Info database landing page](figures/run_info_db-landing_page.png)

The details of a given run for a given user (for local runs of the scouting system this is "scoutdev" or, less often "scoutpro") can be obtained by clicking on the run number.

**Note:** For global runs the data is always stored under the "toppro" user.

An example of the data stored during a local run is given here:

![The configuration data for scouting during a local run](figures/run_info_db-details.png)

**Note:** For a global run it would be necessary to search for the prefix `CMS.l1scout` in order to filter out the information from other systems.

## Example: extract the CMSSW menu used for a run

To obtain the menu used in a given run you will need to find the entry `CMS.l1scout:CONFIG.MENU`. The "value" is a string that contains escaped characters for technical reasons. To make it (reasonably) human-readable you can use Python to write it to a file:

```python
with open("/tmp/scouting_menu.py", "w") as f:
    f.write("import FWCore.ParameterSet.Config as cms\n\nprocess = cms.Process( \"SCHLP\" )\n\nprocess.transferSystem = cms.PSet( \n destinations = cms.vstring( 'Tier0',\n 'Lustre',\n 'None' ), ... ") 
```


![Writing the scouting menu to disk](figures/parse_menu-write.png)
...
![Writing the scouting menu to disk](figures/parse_menu-store.png)

Once executed, you should have the menu in a human-readable format:

![Parsed scouting menu on disk](figures/parsed_menu.png)
