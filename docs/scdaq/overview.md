# SCDAQ

SCDAQ acts as the receiver of scouting data from the processing boards either via DMA (KCU1500 and SB852 boards, legacy) or TCP/IP (VCU128 and potentially DAQ800 in the future).

It is implemented using Intel oneAPI TBB, with a single pipeline acting as the receiver and parallel pipelines working on data processing as well as file output.

Output files are in a format that can be ingested by CMSSW.

<!-- TODO: Describe SCDAQ configuration? -->
<!-- TODO: Describe SCDAQ interface, especially once we move to a Rest-based one. -->
