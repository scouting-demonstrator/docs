# SCONE

SCONE acts as a hardware abstraction layer (with other convenience features) for the L1 scouting project.

It can access scouting boards via PCIe register access and exposes those registers directly via a simple Rest interface. It can also make available board-specific higher-level functions (such as configure, start, and stop) via an extended Rest interface.

In addition, registers are exposed via a Prometheus client for collection as monitoring metrics.

The code is stored in a [Gitlab source control repository](https://gitlab.cern.ch/scouting-demonstrator/scone) where a CI pipeline automatically builds RPMs for deployment at P5.

SCONE is run as a systemd service usually, but can in principle also be run from the command line for e.g. ad-hoc testing.

<!-- TODO: Descibe command line options and how to run like this. -->
<!-- TODO: Describe service file contents? -->
<!-- TODO: Describe Rest interface -->
<!-- TODO: Describe port etc. for Prometheus endpoint. -->
