# 40 MHz scouting on-call guide

## Monitoring the scouting system

For all links in this section access to the .cms network is required.
Follow the steps in the [cluster users
guide](https://twiki.cern.ch/twiki/bin/view/CMS/ClusterUsersGuide#How_to_setup_your_local_machine)
to set this up.

The [new L1 scouting Grafana dashboard](http://l1scout-grafana.cms:3000/) can be used to obtain a quick view of the state of the entire scouting data flow.

<!-- Operation of the board can be monitored at the [L1Scout Grafana
dashboard](http://d3vfu-c2e35-33-01.cms:3000/d/C5bQUsr7z/l1scout). While
both scouting and the upstream system is in a run, we expect the links
to be ***ENABLED*** and "rx bytes aligned" to be ***Aligned*** (the
"cdr" field should mostly be ***stable***, but may flicker at times). In
addition we expect the rates of seen orbits to be fluctuating around 11
kHz (the fluctuation is likely due to the sampling interval) and the
rates of dropped orbits to be zero. Orbit sizes (given in frames) should
be around 2000 for the muon scouting (scoutdaq-s1d12-34-01) and around
1000 for the calorimeter scouting (scoutdaq-s1d12-39-01) when we're in
stable beams. During cosmics data taking the orbit length is expected to
be mostly 17 (1 header frame + 16 trailer frames) with occasionally
larger orbits.

The [SCDAQ monitoring
dashboard](http://d3vfu-c2e35-33-01.cms:3000/d/ezgHg-i4z/scdaq-processes)
provides information on the status of SCDAQ and SCONE. There should be
one SCDAQ process and three SCONE processes running at all times on
production machines. The CPU usage will be higher during stable beams
for the SCDAQ processes and should decrease with LHC luminosity. The
SCONE CPU usage should be fairly constant over time, except if for some
reason the L1 SCOUT function manager is destroyed. (The function manager
queries SCONE regularly for the board status therefore causing a roughly
constant load.) SCDAQ "disk/network writes rate" should roughly
correlate with the SCDAQ CPU usage, it plots the amount of output
written by SCDAQ during a run. The plot of "cache used" will grow while
SCDAQ is taking data. If the cache clearing script is running it should
drop to zero regularly, if not it is expected to hit the maximum and
stay there. It will also drop to zero if SCDAQ is restarted.

The scoutRUBUs are monitored in [their own Grafana
dashboard](http://d3vfu-c2e35-33-01.cms:3000/d/RZn6G5m4z/scoutrubus).
There should be one fileMover process running on all RUBUs that are used
for production at all times. The CPU usage should be spiky during stable
beams data taking but close to zero with occasional spikes while we
aren't taking data. The "NFS activity" should roughly correlate with the
LHC luminosity as should the "Byte rate to Lustre". In contrast the
"File rate in lustre" should be roughly constant whenever we are taking
data (even cosmics, as the files contain a fixed number of orbits).

A more detailed view of the scouting boards link status over time can be
found
[here](http://d3vfu-c2e35-33-01.cms:3000/d/yKE3de3nz/link-status-timeline).
This is especially useful for post mortem investigations. -->

The state of the L1 trigger can be monitored on the [L1T Grafana
instance](http://l1ts-grafana.cms/). This also provides a link to the
[Muon rate
monitor](http://l1ts-grafana.cms/d/LDPFjpeGz/muon-rate-monitor) which
can be helpful to determine whether the muon scouting should expect
input.

In case of problems a good overview can sometimes be gained from the
logs of the function manager. These can be accessed in realtime from any
machine in .cms by executing
`~daqoncall/DAQTools/utilities/HandsawLife.pl -s l1scoutdev -f DEBUG`
for the development FM and
`~daqoncall/DAQTools/utilities/HandsawLife.pl -s l1scoutpro -f DEBUG`
for the production one. For post-mortem analysis you need to log into
the machine `cmsrc-l1scout.cms` and execute
`cat /var/log/rcms/l1scoutpro/Logs_l1scoutpro.xml | ~hsakulin/hs/trunk/Handsaw.pl | less -R`
(replacing `pro` with `dev` when checking the development instance).

## Recovering the system (after: Powercut, reinstallation)

If the system comes back from a power cut or a scoutdaq-type machine has
been reinstalled a few manual steps need to be followed.

### Powercut

After a powercut it is likely that the bitfile needs to be loaded into
the FPGA. This can be done with the command
`$ /usr/sbin/deploy_scouting_firmware.sh [CERN username] [bitfile version] [scouting_board_type] [input_system]`
as described in the section further below. The latest deployed bitfile
can be found in `/var/log/kcu1500_bitfile_deployments.log`, so you can
choose that one, if this is not present (e.g., when the machine was
wiped) you can check the latest release in the [Gitlab
project](https://gitlab.cern.ch/scouting-demonstrator/scouting-preprocessor/-/releases).

Once the bitfile was loaded the machine needs to be rebooted with
`sudo shutdown -r now` as the PCI tree needs to be re-enumerated.

### Reinstallation

After re-installation the following steps need to be performed:

1.  Reload bitfile as described above.
2.  TEMPORARY: Set the scdaq configuration file `/etc/scdaq/scdaq.conf`
    as needed for the given machine. (See `/opt/scdaq/test/config/` for
    examples for a given machine. The fields to change are usually
    "processor\_type", "output\_filename\_prefix",
    "output\_filename\_base", and "nOrbitsPerFile".)
3.  Enable and start SCONE with `sudo systemctl enable --now scone`
4.  Enable and start scdaq with `sudo systemctl enable --now runSCdaq`

## Deploying a bitfile

### KCU1500

Deployment of bitfiles is handled via a script called with
`$ /usr/sbin/deploy_scouting_firmware.sh [CERN username] [bitfile version] [scouting_board_type] [input_system]`,
e.g.

    [dinyar@scoutdaq-s1d12-39-01 ~]$ source /opt/Xilinx/Vivado_Lab/2018.3/settings64.sh
    # Example with bitfile built from branch:
    [dinyar@scoutdaq-s1d12-39-01 ~]$ deploy_scouting_firmware.sh dinyar chore-deploy_via_package_registry-125d3714-dev kcu1500 demux
    # Example with bitfile built from release:
    [dinyar@scoutdaq-s1d12-39-01 ~]$ deploy_scouting_firmware.sh dinyar v1.1.1 kcu1500 demux

***Note:*** In case the board has not been programmed at boot (i.e.
after a power cut) we still need a reboot after programming the FPGA.
This is needed to correctly enumerate the PCI address space.

This script:

- Stops `scdaq` and `SCONE`
- Retrieves the bitfile package from Gitlab and extracts it in ou repository path
- Creates a symlink `/opt/l1scouting-hardware/bitfiles/currently_used that points at the directory of the deployed bitfile
- Loads the bitfile into the FPGA using the script supplied by th bitfile archive, allowing it to perform board-specific tasks (e.g. setting the oscillator correctly and rescanning the PCIe bus)
- Makes note of the bitfile deployment i `/var/log/bitfiles/[scouting_board_type]_deployments.log`
- Starts `SCONE` again
- Resets the board using `SCONE`
- Starts `scdaq`

Correct register parameters that must be set after loading bitfile, as
of March 2023:

On GMT board:

```bash
curl -X POST -F "value=1" localhost:8080/kcu1500_ugmt/orbits_per_packet/write
curl -X POST -F "value=1" localhost:8080/kcu1500_ugmt/reset_board/write
curl -X POST -F "value=0" localhost:8080/kcu1500_ugmt/reset_board/write
```

On Calo board:

```bash
curl -X POST -F "value=1" localhost:8080/kcu1500_demux/orbits_per_packet/write
curl -X POST -F "value=1" localhost:8080/kcu1500_demux/reset_board/write
curl -X POST -F "value=0" localhost:8080/kcu1500_demux/reset_board/write
```

### VCU128

#### After power cycle of the boards/chassis

First step:
```bash
source /opt/Xilinx/Vivado_Lab/2018.3/settings64.sh
deploy_scouting_firmware.sh $USER master-3228b700-dev vcu128 ugmtbmtf 0 1 
deploy_scouting_firmware.sh $USER calo_copy_and_p2gt-49d9000d-dev vcu128 calop2gt 1 1
sudo reboot
```
Note that in the deploy_scouting_firmware script, the first number is the board_index, and the second number is whether it is the first upload of the bitfile after a power cut. 

Excecuting deploy_scouting_firmware.sh without any arguments will give you some helpful info.

If you are prompted to enter your user password, you may also just hit enter to continue. 

Second step:
```bash
export PYTHONPATH=/opt/xdaq/etc/PyHAL/
export LD_LIBRARY_PATH=/opt/xdaq/lib/

## board 0
# on board QSFPs
prog_clock_Si570.py -a /opt/l1scouting-hardware/bitfiles/currently_used/0/address_map_vcu128_ugmtbmtf.dat --devidx 0 -q 1 -f 156.25 -v debug
prog_clock_Si570.py -a /opt/l1scouting-hardware/bitfiles/currently_used/0/address_map_vcu128_ugmtbmtf.dat --devidx 0 -q 2 -f 322.265625 -v debug
prog_clock_Si570.py -a /opt/l1scouting-hardware/bitfiles/currently_used/0/address_map_vcu128_ugmtbmtf.dat --devidx 0 -q 3 -f 322.265625 -v debug
prog_clock_Si570.py -a /opt/l1scouting-hardware/bitfiles/currently_used/0/address_map_vcu128_ugmtbmtf.dat --devidx 0 -q 4 -f 322.265625 -v debug
# mezzanine
prog_clock_Si5341.py -a /opt/l1scouting-hardware/bitfiles/currently_used/0/address_map_vcu128_ugmtbmtf.dat --devidx 0 -f 156.25 -v debug

## board 1
# on board QSFPs
prog_clock_Si570.py -a /opt/l1scouting-hardware/bitfiles/currently_used/1/address_map_vcu128_calop2gt.dat --devidx 1 -q 1 -f 156.25 -v debug
prog_clock_Si570.py -a /opt/l1scouting-hardware/bitfiles/currently_used/1/address_map_vcu128_calop2gt.dat --devidx 1 -q 2 -f 156.25 -v debug
prog_clock_Si570.py -a /opt/l1scouting-hardware/bitfiles/currently_used/1/address_map_vcu128_calop2gt.dat --devidx 1 -q 3 -f 322.265625 -v debug
prog_clock_Si570.py -a /opt/l1scouting-hardware/bitfiles/currently_used/1/address_map_vcu128_calop2gt.dat --devidx 1 -q 4 -f 322.265625 -v debug
# mezzanine
prog_clock_Si5341.py -a /opt/l1scouting-hardware/bitfiles/currently_used/1/address_map_vcu128_calop2gt.dat --devidx 1 -f 156.25 -v debug
```

Third step:
```bash
# reupload bitfiles
source /opt/Xilinx/Vivado_Lab/2018.3/settings64.sh
deploy_scouting_firmware.sh $USER master-5f9668ad-dev vcu128 ugmtbmtf 0 0
deploy_scouting_firmware.sh $USER calo_copy_and_p2gt-49d9000d-dev vcu128 calop2gt 1 0
```

Fourth step:
```bash
# initialize output transceivers
curl -X POST localhost:8080/v2/vcu128_ugmtbmtf/0/initialize
curl -X POST localhost:8080/v2/vcu128_calop2gt/1/initialize
```

#### Reload when a bitfile was already loaded on the boards

First step:
```bash
# reupload bitfiles
source /opt/Xilinx/Vivado_Lab/2018.3/settings64.sh
deploy_scouting_firmware.sh $USER master-5f9668ad-dev vcu128 ugmtbmtf 0 0
deploy_scouting_firmware.sh $USER master-5f9668ad-dev vcu128 ugtcalo 1 0
```

Second step:
```bash
# initialize output transceivers
curl -X POST localhost:8080/v2/vcu128_ugmtbmtf/0/initialize
curl -X POST localhost:8080/v2/vcu128_ugtcalo/1/initialize
```


<!---
## VCU128 commissioning

This section contains all the steps to deploy bitfiles on the two VCU128
boards controlled by `scoutctrl-s1d12-18-01` machine. The connections
are:

-   VCU128 #0:
    -   QUAD 124 -> BMTF #1, #2
    -   QUAD 125 -> BMTF #3, #4
    -   QUAD 126 -> BMTF #5, #6
    -   QUAD 127 -> BMTF #7, #8
    -   QUAD 128 -> BMTF #9, #10
    -   QUAD 129 -> BMTF #11, #12
    -   QUAD 131 -> DAQ unit #0
    -   QUAD 132 -> DAQ unit #1
    -   QUAD 134 -> DAQ unit #2
    -   QUAD 135 -> UGMT final muons
-   VCU128 #1:
    -   QUAD 124 -> UGT
    -   QUAD 125 -> UGT
    -   QUAD 126 -> UGT
    -   QUAD 127 -> UGT
    -   QUAD 128 -> UGT
    -   QUAD 129 -> empty
    -   QUAD 131 -> DAQ unit #0
    -   QUAD 132 -> DAQ unit #1
    -   QUAD 134 -> CALO jets, egammas
    -   QUAD 135 -> CALO sums, taus

All the needed software:

-   Latest SCONE from branch
    <https://gitlab.cern.ch/scouting-demonstrator/scone/-/tree/vcu128-commissioning?ref_type=heads>
-   Latest SCDAQ from branch
    <https://gitlab.cern.ch/scouting-demonstrator/scdaq/-/tree/vcu128-commissioning-feb?ref_type=heads>
-   Latest scouting-hardware-tools from branch
    <https://gitlab.cern.ch/scouting-demonstrator/scouting-hardware-tools/-/tree/rardino-vcu128-deployment-tools?ref_type=heads>

All the needed firmware:

-   Latest version of scouting-preprocessor from branch
    <https://gitlab.cern.ch/scouting-demonstrator/scouting-preprocessor/-/tree/scouting_pipelines?ref_type=heads>
    (in package registry: `scouting_pipelines-fcba2277-dev`)

### Bitfile upload after power cycle of the boards/chassis

The VCU128 boards have no PCIe core in the default bitfile, so the first
step is loading the bitfile and then reboot so that the PCIe core
appears on the PCI tree. The deployment of the bitfile is handled by a
script. For example

    $ /usr/sbin/deploy_scouting_firmware.sh [CERN username] [bitfile version] [scouting_board_type] [input_system] [board_index] [first_upload]

So, we will need to set `first_upload` argument to 1. As firmware
version, take the latest build from the dev branch with the "scouting
pipelines" and the orbit boundaries reformatting patch `18a93545`.

    source /opt/Xilinx/Vivado_Lab/2018.3/settings64.sh
    deploy_scouting_firmware.sh rardino scouting_pipelines-18a93545-dev vcu128 ugmtbmtf 0 1
    deploy_scouting_firmware.sh rardino scouting_pipelines-18a93545-dev vcu128 ugtcalo 1 1
    sudo reboot

### MGT clocks reprogramming after first bitfile upload and reboot

Let's assume that we need to reprogram the MGT clocks of the VCU128
mezzanine and/or of the on-board QSFPs. There are two scripts, one for
the mezzanine and one for the on-board QSFPs:

For the on-board QSFPs

    export PYTHONPATH=/opt/xdaq/etc/PyHAL/
    export LD_LIBRARY_PATH=/opt/xdaq/lib/
    prog_clock_Si570.py -a ADDRMAP --vendid VENDOR_ID --devid DEVICE_ID --devidx BOARD_INDEX -q QSFP_NUMBER -f NEW_FREQUENCY -v VERBOSITY

For the mezzanine:

    export PYTHONPATH=/opt/xdaq/etc/PyHAL/
    export LD_LIBRARY_PATH=/opt/xdaq/lib/
    prog_clock_Si5341.py -a ADDRMAP --vendid VENDOR_ID --devid DEVICE_ID --devidx BOARD_INDEX -f NEW_FREQUENCY -v VERBOSITY

Use the following configuration/commands for VCU128 #0:

    export PYTHONPATH=/opt/xdaq/etc/PyHAL/
    export LD_LIBRARY_PATH=/opt/xdaq/lib/
    # on board QSFPs
    prog_clock_Si570.py -a /opt/l1scouting-hardware/bitfiles/currently_used/0/address_map_vcu128_ugmtbmtf.dat --devidx 0 -q 1 -f 156.25 -v debug
    prog_clock_Si570.py -a /opt/l1scouting-hardware/bitfiles/currently_used/0/address_map_vcu128_ugmtbmtf.dat --devidx 0 -q 2 -f 322.265625 -v debug
    prog_clock_Si570.py -a /opt/l1scouting-hardware/bitfiles/currently_used/0/address_map_vcu128_ugmtbmtf.dat --devidx 0 -q 3 -f 322.265625 -v debug
    prog_clock_Si570.py -a /opt/l1scouting-hardware/bitfiles/currently_used/0/address_map_vcu128_ugmtbmtf.dat --devidx 0 -q 4 -f 322.265625 -v debug
    # mezzanine
    prog_clock_Si5341.py -a /opt/l1scouting-hardware/bitfiles/currently_used/0/address_map_vcu128_ugmtbmtf.dat --devidx 0 -f 156.25 -v debug

Use the following configuration/commands for VCU128 #1:

    export PYTHONPATH=/opt/xdaq/etc/PyHAL/
    export LD_LIBRARY_PATH=/opt/xdaq/lib/
    # on board QSFPs
    prog_clock_Si570.py -a /opt/l1scouting-hardware/bitfiles/currently_used/1/address_map_vcu128_ugtcalo.dat --devidx 0 -q 1 -f 156.25 -v debug
    prog_clock_Si570.py -a /opt/l1scouting-hardware/bitfiles/currently_used/1/address_map_vcu128_ugtcalo.dat --devidx 0 -q 2 -f 156.25 -v debug
    prog_clock_Si570.py -a /opt/l1scouting-hardware/bitfiles/currently_used/1/address_map_vcu128_ugtcalo.dat --devidx 0 -q 3 -f 322.265625 -v debug
    prog_clock_Si570.py -a /opt/l1scouting-hardware/bitfiles/currently_used/1/address_map_vcu128_ugtcalo.dat --devidx 0 -q 4 -f 322.265625 -v debug
    # mezzanine
    prog_clock_Si5341.py -a /opt/l1scouting-hardware/bitfiles/currently_used/1/address_map_vcu128_ugtcalo.dat --devidx 0 -f 156.25 -v debug

Now, we need to reupload the bitfile since all the PLLs need to be
locked at boot time.

### Bitfile re-upload after first upload+reboot or after clock reprogramming or in general if device is already listed on PCIe tree

Same as before, but the `first_upload` argument has to be set to 0.

    source /opt/Xilinx/Vivado_Lab/2018.3/settings64.sh
    deploy_scouting_firmware.sh rardino scouting_pipelines-18a93545-dev vcu128 ugmtbmtf 0 0
    deploy_scouting_firmware.sh rardino scouting_pipelines-18a93545-dev vcu128 ugtcalo 1 0

### Configuring with SCONE

After all the previous steps have been performed, time to reset,
configure and start. The destination IPs will be set to the one of
`scoutrubu-c2e37-29-01` machine. In this example, we are enabling only
`ugmt` and `calo`, while `ugmt` and `ugt` are disabled. Beware, all the
streams sent to the same RUBUs need a different destination port in
order for SCDAQ to receive them correctly.

```bash
# reset boards
curl -X POST localhost:8080/v2/vcu128_ugmtbmtf/0/reset
curl -X POST localhost:8080/v2/vcu128_ugtcalo/1/reset
# configure
curl -X POST -H "Content-type: application/json" -d '{"enabled_systems":["ugmt"], "sourceIp":["10.177.128.190", "10.177.128.191", "10.177.128.192"], "destIp":["10.177.128.185", "10.177.128.185", "10.177.128.185"], "sourcePort" : [[10000], [10000, 10010, 10020, 10030, 10040, 10050], [10000, 10010, 10020, 10030, 10040, 10050]], "destPort"   : [[10000], [10000, 10010, 10020, 10030, 10040, 10050], [10000, 10010, 10020, 10030, 10040, 10050]]}' localhost:8080/v2/vcu128_ugmtbmtf/0/configure
curl -X POST -H "Content-type: application/json" -d '{"enabled_systems":["calo"], "sourceIp":["10.177.128.194", "10.177.128.195"], "destIp":["10.177.128.185", "10.177.128.185"], "sourcePort" : [[10000], [10000, 10010, 10020, 10030]], "destPort"   : [[10000], [10010, 10020, 10030, 10040]]}' localhost:8080/v2/vcu128_ugtcalo/1/configure
```

After SCDAQ has been started on the relevant RUBUs where the destination
IP is pointing to (see next subsection), start the boards:

```bash
curl -X POST localhost:8080/v2/vcu128_ugmtbmtf/0/start
curl -X POST localhost:8080/v2/vcu128_ugtcalo/1/start
```

### Running SCDAQ for the test with GMT+CALO

Assuming CMS is running and in a good state, let's take for example as
receiving RUBU the `scoutrubu-c2e37-29-01`. We will need to run here a
dev version of scdaq with all the new dedicated processors for the TCP
streams sent by the VCU128s and with the new decoding of the orbit
trailer (simplified). Note that we will run scdaq locally and not as a
service since few changes and json config parsing are needed to move to
scdaq as a service.

```bash
kinit ${USER}@CERN.CH
git clone https://:@gitlab.cern.ch:8443/scouting-demonstrator/scdaq.git -b vcu128-commissioning-feb
cd scdaq
mkdir build && cd "$_"
cmake ..
make -j12
cd ..
```

We need to create the configuration files using a script available in
the repository. Depending on the source choice, some argument will not
be considered:

```bash
# source scripts/generate_configs.sh [SOURCE] [SCONE_BOARD] [MIN_SOURCE_ID] [MIN_TCP_DEST_PORT] [NSTREAMS] [OUTPUT_FORCE_WRITE] [LOGLEVEL] [RC_PORT]
source scripts/generate_configs.sh ugmtcalo ugmtbmtf 1 10000 5 yes DEBUG 8000
```

Now we can start SCDAQ, and use the "/start" endpoint on the
`scoutctrl-s1d12-18-01` control server.

```
sudo build/scdaq --config test/config/scdaq-ugmtcalo.conf --nstreams 5
```

If everything went well and if CMS is Running (or at least TRG is
configured), files will start to appear under
`/fff/ramdisk/BU0/run000000/`. Don't be scared if unexpected files
`run000000_ls0001_index000000.raw*` appear :), since the first packet
sent by the VCU128 is empty and with orbit number 0, so this produces
the previous files. The fix is simple and there is a TODO in the scdaq
processor code in the lines where it should be applied, but it is not
necessary at the moment.
-->


## Restart grafana or prometheus service

To restart prometheus service:
On d3vfu-c2e35-33-02 `sudo service prometheus start`

To restart the grafana dashboard:
On d3vfu-c2e35-33-01 `sudo service grafana-server start`

## Tests

### Deploy a test version of SCDAQ

To deploy a test version of SCDAQ Puppet can be disabled. You should do
so with

```bash
sudo /usr/local/bin/maintenance.sh -d -m "([your initials go here]) testing something"
```

and can then install the test RPM with yum. To re-enable Puppet again
you can use:

```bash
sudo /usr/local/bin/maintenance.sh -e -c
```

force puppet to re-run with
```bash
sudo puppet agent -t
```

## Recover from fatal crashes

First stop boards on scoutctrl-s1d12-18-01 with
```bash
curl -X POST localhost:8080/v2/vcu128_ugmtbmtf/0/stop
curl -X POST localhost:8080/v2/vcu128_ugtcalo/1/stop
```

then to clear rubus and fus run on the 'main' rubu
```bash
touch /fff/ramdisk/tsunami_all
```


